﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace BikeRental.Model
{
    public class Customer : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

    //initialize private attributes
    private int _customerId;
    private string _firstName;
    private string _lastName;
    private SexGender _sex;
    private double _height;
    private int _age;
    private string _email;
    private string _city;
    private ObservableCollection<Bike> _bikes;
    private Boolean _isEnabled;
    //make public getters and setters with a notify function to trigger changes
    public int CustomerId
    {
        get => _customerId;
        set
        {
            _customerId = value;
            Notify("CustomerId");
        }
    }
    public string FirstName
    {
        get => _firstName;
        set
        {
            _firstName = value;
            Notify("FirstName");
        }
    }
    public string LastName
    {
        get => _lastName;
        set
        {
            _lastName = value;
            Notify("LastName");
        }
    }
    public SexGender Sex
    {
        get => _sex;
        set
        {
            _sex = value;
            Notify("Sex");
        }
    }

    public double Height
    {
        get => _height;
        set
        {
            _height = value;
            Notify("Height");
        }
    }

    public int Age
    {
        get => _age;
        set
        {
            _age = value;
            Notify("Age");
        }
    }

    public string Email
    {
        get => _email;
        set
        {
            _email = value;
            Notify("Email");
        }
    }

    public string City
    {
        get => _city;
        set
        {
            _city = value;
            Notify("City");
        }
    }

    public ObservableCollection<Bike> Bikes
    {
        get => _bikes;
        set
        {
            _bikes = value;
            Notify("Bikes");
        }
    }

    public Boolean IsEnabled
    {
        get => _isEnabled;
        set
        {
            _isEnabled = value;
            Notify("IsEnabled");
        }
    }

    //Constructor
    public Customer(Boolean trueOrFalse = false)
    {
        IsEnabled = trueOrFalse;
    }

    public void Notify(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    }
    public enum SexGender
    {
        Man,
        Vrouw,
        Anders
    }
}