﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace BikeRental.Model
{
    public class Bike : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        //initialize private attributes
        private int _bikeId;
        private string _brand;
        private string _color;
        private BikeType _bikeType;
        private SexBike _sex;
        private int _hourRate;
        private int _dailyRate;
        private int _size;
        private Boolean _isEnabled;
        //make public getters and setters with a notify function to trigger changes
        public int BikeId { get => _bikeId; set { _bikeId = value; Notify("BikeId"); } }
        public string Brand { get => _brand; set { _brand = value; Notify("Brand"); } }
        public string Color { get => _color; set { _color = value; Notify("Color"); } }
        public BikeType BikeType { get => _bikeType; set { _bikeType = value; Notify("BikeType"); } }
        public SexBike Sex { get => _sex; set { _sex = value; Notify("Sex"); } }
        public int HourRate { get => _hourRate; set { _hourRate = value; Notify("HourRate"); } }
        public int DailyRate { get => _dailyRate; set { _dailyRate = value; Notify("DailyRate"); } }
        public int Size { get => _size; set { _size = value; Notify("Size"); } }
        public Boolean IsEnabled { get => _isEnabled; set { _isEnabled = value; Notify("IsEnabled"); } }
        //Constructor 
        public Bike(Boolean trueOrFalse = false)
        {
            IsEnabled = trueOrFalse;
        }
        public void Notify(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
}
    public enum BikeType
    {
        Racefietsen,
        Mountainbike,
        Crossfietsen,
        Standsfietsen,
        Sportfietsen
    }
    public enum SexBike
    {
        Man,
        Vrouw,
        Jongens,
        Meisjes,
        Genderneutraal,
        MagicMike
    }
}