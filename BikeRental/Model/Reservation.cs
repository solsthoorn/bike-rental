﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeRental.Model
{
    public class Reservation : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        //initialize private attributes
        private Customer _customer;
        private Bike _bike;
        private DateTime _start;
        private DateTime _end = DateTime.MinValue;
        private Retailer _pickUp;
        private Retailer _dropOff;
        //make public getters and setters with a notify function to trigger changes
        public Customer Customer { get => _customer; set { _customer = value; Notify("Customer"); } }
        public Bike Bike { get => _bike; set { _bike = value; Notify("Bike"); } }
        public DateTime Start { get => _start; set { _start = value; Notify("Start"); } }
        public DateTime End { get => _end; set { _end = value; Notify("End"); } }
        public Retailer PickUp { get => _pickUp; set { _pickUp = value; Notify("PickUp"); } }
        public Retailer DropOff { get => _dropOff; set { _dropOff = value; Notify("DropOff"); } }
        public int ReservationId { get; set; } = 0;

        public Reservation(Boolean needCountUp = false)
        {
            if(needCountUp == true)
            {
                ReservationId = ReservationId + 1;
            }
        }
        public void MakeReservation(Customer selectedCustomer, Bike selectedBike, Retailer selectedRetailer)
        {
            Customer = selectedCustomer;
            Bike = selectedBike;
            Start = DateTime.Now;
            PickUp = selectedRetailer;
        }
        public void UpdateReservation(Reservation currentReservation, Retailer selectedRetailer)
        {
            currentReservation.DropOff = selectedRetailer;
            currentReservation.End = DateTime.Now;
        }
        public void Notify(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
