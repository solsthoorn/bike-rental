﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeRental.Model
{
    public class Retailer : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        //initialize private attributes
        private int _retailerId;
        private string _name;
        private string _address;
        private string _city;
        private int _maxCapacity;
        private ObservableCollection<Bike> _bikes;
        private Boolean _isEnabled;
        //make public getters and setters with a notify function to trigger changes
        public int RetailerId { get => _retailerId; set { _retailerId = value; Notify("RetailerId"); } }
        public string Name { get => _name; set { _name = value; Notify("Name"); } }
        public string Address { get => _address; set { _address = value; Notify("Address"); } }
        public string City { get => _city; set { _city = value; Notify("City"); } }
        public int MaxCapacity { get => _maxCapacity; set { _maxCapacity = value; Notify("MaxCapacity"); } }
        public ObservableCollection<Bike> Bikes { get => _bikes; set { _bikes = value; Notify("Bikes"); } }
        public Boolean IsEnabled { get => _isEnabled; set { _isEnabled = value; Notify("IsEnabled"); } }
        //constructor
        public Retailer(Boolean trueOrFalse = false)
        {
            IsEnabled = trueOrFalse;
        }
        public void Notify(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
