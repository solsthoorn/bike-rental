﻿using BikeRental.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BikeRental.Class
{
    public class Validator
    {
        public Boolean ControllListBoxItemsOfBikes(Bike selectedBike, Retailer selectedRetailer)
        {
            Boolean error = true;
            if (selectedBike == null)
            {
                MessageBox.Show("Sorry, maar u moet eerst een fiets selecteren");
            } 
            else if (selectedRetailer == null)
            {
                MessageBox.Show("Sorry, maar u moet eerst een winkel selecteren");
            }
            else
            {
                int currentBikes = 0;
                if (selectedRetailer.Bikes != null)
                {
                    currentBikes = selectedRetailer.Bikes.Count();
                }
                if (selectedRetailer.MaxCapacity == currentBikes)
                {
                    MessageBox.Show("Sorry maar u heeft het limiet van " + selectedRetailer.MaxCapacity + " behaald");
                }
                else
                {
                    error = false;
                }
            }
            return error;
        }
        public Boolean CheckUser(Customer selectedCustomer)
        {
            Boolean error = true;
            if(selectedCustomer == null)
            {
                MessageBox.Show("U moet eerst een klant selecteren");
            }
            else
            {
                error = false;
            }
            return error;
        }
    }
}
