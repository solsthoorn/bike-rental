﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BikeRental.Model;

namespace BikeRental.Class
{
    public class FilterOut
    {
        public ObservableCollection<Bike> FilterOutBikes(ObservableCollection<Bike> bikeList, ObservableCollection<Retailer> retailerList)
        {
            ObservableCollection<Bike> filteredList = new ObservableCollection<Bike>(bikeList);
            foreach (Retailer retailerRow in retailerList)
            {
                if(retailerRow.Bikes != null)
                {
                    foreach (Bike bikeRow in retailerRow.Bikes)
                    {
                        filteredList.Remove(bikeRow);
                    }
                }
            }
            return filteredList;
        }

    }
}
