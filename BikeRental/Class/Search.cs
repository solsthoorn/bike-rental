﻿using BikeRental.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BikeRental.Class
{
    public static class Search
    {
        public static int SearchReservation(ObservableCollection<Reservation> reservations, Bike bike, Customer customer)
        {
            int index = -1;
            int counter = 0;
            foreach(Reservation reservation in reservations)
            {
                if (reservation.End == DateTime.MinValue)
                {
                    if (reservation.Customer.CustomerId == customer.CustomerId)
                    {
                        if (reservation.Bike.BikeId == bike.BikeId)
                        {
                            index = counter;
                        }
                    }
                }
                counter++;
            }
            return index;
        }
        public static int SearchForEmptyIdByRetailer(ObservableCollection<Retailer> retailers)
        {
            int newRetailerId = 0;
            int countRetailers = retailers.Count();
            Retailer lastRetailer = retailers[countRetailers - 1];
            newRetailerId = lastRetailer.RetailerId + 1;
            return newRetailerId;
        }
        public static int SearchForEmptyIdByBike(ObservableCollection<Bike> bikes)
        {
            int newBikeId = 0;
            int countBikes = bikes.Count();
            Bike lastBike = bikes[countBikes - 1];
            newBikeId = lastBike.BikeId + 1;
            return newBikeId;
        }
        public static int SearchForEmptyIdByCustomer(ObservableCollection<Customer> customers)
        {
            int newCustomerId = 0;
            int countCustomers = customers.Count();
            Customer lastCustomer = customers[countCustomers - 1];
            newCustomerId = lastCustomer.CustomerId + 1;
            return newCustomerId;
        }
    }
}