﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BikeRental.Class;
using BikeRental.Model;

namespace BikeRental.ViewModel
{
    public class RentingViewModel : INotifyPropertyChanged
    {
        private Boolean _isEnabledSelectSelectedAddBike;
        private Boolean _isEnabledConnectBike;
        private int _selectSelectedAddBike;
        private int _selectSelectedRemoveBike;

        private Customer _selectedCustomer;
        private Bike _selectedBike;
        private Retailer _selectedRetailer;
        private ObservableCollection<Bike> _listOfBikesToInsert;

        public ObservableCollection<Bike> ShowBikes { get; set; }
        public ObservableCollection<Customer> Customers { get; set; }
        public ObservableCollection<Bike> Bikes { get; set; }
        public ObservableCollection<Retailer> Retailers { get; set; }
        public ObservableCollection<Reservation> Reservations { get; set; }
        public ObservableCollection<Bike> ListOfBikesToInsert { get => _listOfBikesToInsert; set { _listOfBikesToInsert = value; Notify("ListOfBikesToInsert"); } }

        public RelayCommand RemoveBikeFromCustomerClick { get; set; }
        public RelayCommand AddBikeToCustomerClick { get; set; }
        public Boolean IsEnabledSelectSelectedAddBike { get => _isEnabledSelectSelectedAddBike; set { _isEnabledSelectSelectedAddBike = value; Notify("IsEnabledSelectSelectedAddBike"); } }
        public Boolean IsEnabledConnectBike { get => _isEnabledConnectBike; set { _isEnabledConnectBike = value; Notify("IsEnabledConnectBike"); } }
        public Customer SelectedCustomer { get => _selectedCustomer; set { _selectedCustomer = value; Notify("SelectedCustomer"); } }
        public Bike SelectedBike { get => _selectedBike; set { _selectedBike = value; Notify("SelectedBike"); } }
        
        public event PropertyChangedEventHandler PropertyChanged;

        public int SelectSelectedAddBike
        {

            get { return _selectSelectedAddBike; }
            set
            {
                _selectSelectedAddBike = value;
                _selectSelectedRemoveBike = -1;
                IsEnabledConnectBike = true;
            }
        }
        public int SelectSelectedRemoveBike
        {
            get { return _selectSelectedRemoveBike; }
            set
            {
                _selectSelectedRemoveBike = value;
                _selectSelectedAddBike = -1;
                IsEnabledConnectBike = false;
            }
        }
        public Retailer SelectedRetailer
        {
            get => _selectedRetailer; set
            {
                _selectedRetailer = value;
                Notify("SelectedRetailer");
                FilterOut filterOut = new FilterOut();
                ListOfBikesToInsert = filterOut.FilterOutBikes(Bikes, Retailers);
            }
        }
        public RentingViewModel(ObservableCollection<Customer> customersList, ObservableCollection<Bike> bikeList, ObservableCollection<Retailer> retailersList, ObservableCollection<Reservation> reservationList)
        {
            RemoveBikeFromCustomerClick = new RelayCommand(RemoveBike);
            AddBikeToCustomerClick = new RelayCommand(AddBike);
            Customers = customersList;
            Bikes = bikeList;
            Retailers = retailersList;
            ListOfBikesToInsert = new ObservableCollection<Bike>(bikeList);
            Reservations = reservationList;
        }

        public void AddBike(object obj)
        {
            Validator valid = new Validator();
            Boolean allowed = valid.ControllListBoxItemsOfBikes(SelectedBike, SelectedRetailer);
            if(allowed == false)
            {
                if(valid.CheckUser(SelectedCustomer) == false)
                {
                    if (SelectedCustomer.Bikes == null)
                    {
                        SelectedCustomer.Bikes = new ObservableCollection<Bike> { SelectedBike };
                    }
                    else
                    {
                        SelectedCustomer.Bikes.Add(SelectedBike);
                    }
                    Reservation makeReservation = new Reservation(true);
                    makeReservation.MakeReservation(SelectedCustomer, SelectedBike, SelectedRetailer);
                    Reservations.Add(makeReservation);
                    SelectedRetailer.Bikes.Remove(SelectedBike);
                }
            }
        }
        public void RemoveBike(object obj)
        {
            Validator valid = new Validator();
            Boolean allowed = valid.ControllListBoxItemsOfBikes(SelectedBike, SelectedRetailer);
            if (allowed == false)
            {
                if (valid.CheckUser(SelectedCustomer) == false)
                {
                    int reservationIndex = Search.SearchReservation(Reservations, SelectedBike, SelectedCustomer);
                    if(reservationIndex == -1)
                    {
                        MessageBox.Show("Wij hebben uw reserving niet kunenn vinden");
                    }
                    else
                    {
                        Reservation makeReservation = new Reservation(true);
                        makeReservation.UpdateReservation(Reservations[reservationIndex], SelectedRetailer);
                        SelectedRetailer.Bikes.Add(SelectedBike);
                        SelectedCustomer.Bikes.Remove(SelectedBike);
                    }
                }
            }
        }
        private void Notify(string v)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(v));
        }
    }
}
