﻿using BikeRental.Class;
using BikeRental.Model;
using BikeRental.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BikeRental.ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private Bike _selectedBike;
        private Retailer _selectedRetailer;
        private Customer _selectedCustomer;
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Bike> Bikes { get; set; }
        public ObservableCollection<Bike> ShowBikes { get; set; }
        public ObservableCollection<Retailer> Retailers { get; set; }
        public ObservableCollection<Customer> Customers { get; set; }
        public ObservableCollection<Reservation> Reservations { get; set; }
        public Bike SelectedBike { get => _selectedBike; set { _selectedBike = value; Notify("SelectedBike"); } }
        public Retailer SelectedRetailer { get => _selectedRetailer; set { 
                _selectedRetailer = value;
                ShowBikes = SelectedRetailer.Bikes;
                Notify("SelectedRetailer");
                Notify("ShowBikes");
            }
        }
        public Customer SelectedCustomer { get => _selectedCustomer; set { 
                _selectedCustomer = value;
                ShowBikes = SelectedCustomer.Bikes;
                Notify("SelectedCustomer");
                Notify("ShowBikes");
            } 
        }
        public RelayCommand goToBikesClick { get; set; }
        public RelayCommand goToRetailersClick { get; set; }
        public RelayCommand goToCustomersClick { get; set; }
        public RelayCommand RentABikeClick { get; set; }
        public RelayCommand GoToReservationsClick { get; set; }
        public MainViewModel()
        {
            goToBikesClick = new RelayCommand(goToBikes);
            goToRetailersClick = new RelayCommand(goToRetailers);
            goToCustomersClick = new RelayCommand(goToCustomers);
            RentABikeClick = new RelayCommand(goToRenting);
            GoToReservationsClick = new RelayCommand(GoToReservations);
            //Make starting instances
            Bikes = new ObservableCollection<Bike> {
                new Bike()
                {
                    BikeId = 1,
                    Brand = "Sparta",
                    Color = "Black",
                    BikeType = BikeType.Mountainbike,
                    Sex = SexBike.Genderneutraal,
                    HourRate = 10,
                    DailyRate = 2,
                    Size = 180
                },
                new Bike()
                {
                    BikeId = 2,
                    Brand = "Spirit",
                    Color = "Blue",
                    BikeType = BikeType.Mountainbike,
                    Sex = SexBike.Man,
                    HourRate = 40,
                    DailyRate = 4,
                    Size = 120
                },
                new Bike()
                {                    
                    BikeId = 3,
                    Brand = "Goos",
                    Color = "Yellow",
                    BikeType = BikeType.Standsfietsen,
                    Sex = SexBike.MagicMike,
                    HourRate = 20,
                    DailyRate = 2,
                    Size = 20
                },
                new Bike()
                {
                    BikeId = 4,
                    Brand = "Dog",
                    Color = "Red",
                    BikeType = BikeType.Crossfietsen,
                    Sex = SexBike.Jongens,
                    HourRate = 45,
                    DailyRate = 42,
                    Size = 1210
                },
                new Bike()
                {
                    BikeId = 5,
                    Brand = "Cat",
                    Color = "Green",
                    BikeType = BikeType.Racefietsen,
                    Sex = SexBike.Meisjes,
                    HourRate = 42,
                    DailyRate = 14,
                    Size = 130
                },
                new Bike()
                {
                    BikeId = 6,
                    Brand = "Hert",
                    Color = "Pink",
                    BikeType = BikeType.Mountainbike,
                    Sex = SexBike.Meisjes,
                    HourRate = 12,
                    DailyRate = 42,
                    Size = 130
                },
                new Bike()
                {
                    BikeId = 7,
                    Brand = "Gazelle",
                    Color = "Blue",
                    BikeType = BikeType.Crossfietsen,
                    Sex = SexBike.Jongens,
                    HourRate = 12,
                    DailyRate = 30,
                    Size = 80
                },
                new Bike()
                {
                    BikeId = 8,
                    Brand = "Berkelaar",
                    Color = "White",
                    BikeType = BikeType.Standsfietsen,
                    Sex = SexBike.Genderneutraal,
                    HourRate = 5,
                    DailyRate = 20,
                    Size = 152
                }
            };
            Retailers = new ObservableCollection<Retailer> {
                new Retailer()
                {
                    RetailerId = 1,
                    Name = "fiet aan huis",
                    Address = "Viergang 89",
                    City = "Mijdrecht",
                    MaxCapacity = 5,
                    Bikes = new ObservableCollection<Bike> {
                        Bikes[5], 
                        Bikes[6]   
                    }
                },
                new Retailer()
                {
                    RetailerId = 2,
                    Name = "Windesheim",
                    Address = "Hospitaaldreef 5",
                    City = "Almere",
                    MaxCapacity = 10,
                    Bikes = new ObservableCollection<Bike> {
                        Bikes[7]
                    }
                }
            };
            Customers = new ObservableCollection<Customer>
            {
                new Customer()
                {
                    CustomerId = 1,
                    FirstName = "Babzooka",
                    LastName = "Bananaa",
                    Sex = SexGender.Man,
                    Height = 1.10,
                    Age = 666,
                    Email = "Explosion@@@windows.net",
                    City = "Mexico City",
                    Bikes = new ObservableCollection<Bike>
                    {
                        Bikes[0],
                        Bikes[4]
                    }
                },
                new Customer()
                {
                    CustomerId = 2,
                    FirstName = "Mike",
                    LastName = "Magic",
                    Sex = SexGender.Vrouw,
                    Height = 2.10,
                    Age = 9,
                    Email = "mike@vanderbijl.nl",
                    City = "theezakjestad",
                    Bikes = new ObservableCollection<Bike>
                    {
                        Bikes[1]
                    }
                },
                new Customer()
                {
                    CustomerId = 3,
                    FirstName = "Binskoepie",
                    LastName = "tifusjantjes",
                    Sex = SexGender.Man,
                    Height = 1.10,
                    Age = 69,
                    Email = "Janmetdekorte@achternaam.nl",
                    City = "SpagettiCity",
                    Bikes = new ObservableCollection<Bike>
                    {
                        Bikes[3]
                    }
                },
                new Customer()
                {
                    CustomerId = 4,
                    FirstName = "Peter",
                    LastName = "Voel me stukke beter",
                    Sex = SexGender.Man,
                    Height = 2.25,
                    Age = 15,
                    Email = "Hannes@outlook.cunt",
                    City = "MyHomeTown",
                    Bikes = new ObservableCollection<Bike>
                    {
                        Bikes[2]
                    }
                },
                new Customer()
                {
                    CustomerId = 5,
                    FirstName = "Zoro",
                    LastName = "Malboro",
                    Sex = SexGender.Man,
                    Height = 1.10,
                    Age = 25,
                    Email = "Handige@ali.nl",
                    City = "MyHomeTown",
                    Bikes = new ObservableCollection<Bike>{}
                },
                new Customer()
                {
                    CustomerId = 6,
                    FirstName = "Boerenzoon",
                    LastName = "Aliantie",
                    Sex = SexGender.Man,
                    Height = 1.80,
                    Age = 29,
                    Email = "Borroti@sambali.nl",
                    City = "Almelo",
                    Bikes = new ObservableCollection<Bike>{}
                }
            };
            Reservations = new ObservableCollection<Reservation>
            {
                new Reservation()
                {
                    Customer = Customers[0],
                    Bike = Bikes[0],
                    Start = DateTime.Now,
                    PickUp = Retailers[0]
                },
                new Reservation()
                {
                    Customer = Customers[0],
                    Bike = Bikes[4],
                    Start = DateTime.Now,
                    PickUp = Retailers[0]
                },
                new Reservation()
                {
                    Customer = Customers[1],
                    Bike = Bikes[1],
                    Start = DateTime.Now,
                    PickUp = Retailers[1]
                },
                new Reservation()
                {
                    Customer = Customers[2],
                    Bike = Bikes[3],
                    Start = DateTime.Now,
                    PickUp = Retailers[1]
                },
                new Reservation()
                {
                    Customer = Customers[3],
                    Bike = Bikes[2],
                    Start = DateTime.Now,
                    PickUp = Retailers[1]
                },
            };
        }
        public void goToBikes(object a)
        {
            BikesView bv = new BikesView(Bikes);
            bv.Show();
        }
        public void goToRetailers(object a)
        {
            RetailersView rv = new RetailersView(Bikes, Retailers);
            rv.Show();
        }
        public void goToCustomers(object a)
        {
            CustomerView cv = new CustomerView(Customers, Bikes);
            cv.Show();
        }
        public void goToRenting(object a)
        {
            RentingView rv = new RentingView(Customers, Bikes, Retailers, Reservations);
            rv.Show();
        }
        public void GoToReservations(object a)
        {
            ReservationView rv = new ReservationView(Customers, Bikes, Retailers, Reservations);
            rv.Show();
        }
        private void Notify(string v)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(v));
        }
    }
}
