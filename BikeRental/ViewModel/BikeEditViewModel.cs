﻿using BikeRental.Class;
using BikeRental.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BikeRental.ViewModel
{
    public class BikeEditViewModel : INotifyPropertyChanged
    {
        //variables
        private Bike _selectedBike;
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Bike> Bikes { get; set; }
        public Bike SelectedBike { get => _selectedBike; set { _selectedBike = value; Notify("SelectedBike"); } }
        //relay functions
        public RelayCommand AddBikeClick { get; set; }
        public RelayCommand ClearFormClick { get; set; }
        public RelayCommand DeleteBikeClick { get; set; }

        public BikeEditViewModel(ObservableCollection<Bike> bikeList)
        {
            AddBikeClick = new RelayCommand(AddBike);
            ClearFormClick = new RelayCommand(ClearForm);
            DeleteBikeClick = new RelayCommand(DeleteBike);
            SelectedBike = new Bike(true);
            Bikes = bikeList;
        }
        public void ClearForm(object a)
        {
            SelectedBike = new Bike(true);
        }
        public void AddBike(object a)
        {
            SelectedBike.BikeId = Search.SearchForEmptyIdByBike(Bikes);
            SelectedBike.IsEnabled = false;
            Bikes.Add(SelectedBike);
            SelectedBike = new Bike(true);
        }
        public void DeleteBike(object a)
        {
            Bikes.Remove(SelectedBike);
            SelectedBike = new Bike(true);

        }
        public IList<BikeType> TypeList
        {
            get
            {
                return Enum.GetValues(typeof(BikeType)).Cast<BikeType>().ToList<BikeType>();
            }
        }
        public IList<SexBike> SexList
        {
            get
            {
                return Enum.GetValues(typeof(SexBike)).Cast<SexBike>().ToList<SexBike>();
            }
        }
        private void Notify(string v)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(v));
        }
    }
}