﻿using BikeRental.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeRental.ViewModel
{
    public class ReservationsViewModel
    {
        public ObservableCollection<Customer> Customers { get; set; }
        public ObservableCollection<Bike> Bikes { get; set; }
        public ObservableCollection<Retailer> Retailers { get; set; }
        public ObservableCollection<Reservation> Reservations { get; set; }
        public ReservationsViewModel(ObservableCollection<Customer> customerList, ObservableCollection<Bike> bikeList, ObservableCollection<Retailer> retailerList, ObservableCollection<Reservation> reservationList)
        {
            Customers = customerList;
            Bikes = bikeList;
            Retailers = retailerList;
            Reservations = reservationList;
        }
    }
}