﻿using BikeRental.Class;
using BikeRental.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BikeRental.ViewModel
{
    public class RetailerEditViewModel : INotifyPropertyChanged
    {
        //variables
        private int _selectSelectedAddBike;
        private int _selectSelectedRemoveBike;
        private Retailer _selectedRetailer;
        private Bike _selectedBike;
        private Boolean _isEnabledSelectSelectedAddBike;
        private Boolean _isEnabledConnectBike;
        private ObservableCollection<Bike> _listOfBikesToInsert;
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Retailer> Retailers { get; set; }
        public ObservableCollection<Bike> Bikes { get; set; }
        public ObservableCollection<Bike> ListOfBikesToInsert { get => _listOfBikesToInsert; set { _listOfBikesToInsert = value; Notify("ListOfBikesToInsert"); } }
        public Bike SelectedBike { get => _selectedBike; set { _selectedBike = value; Notify("SelectedBike"); } }
        public Boolean IsEnabledSelectSelectedAddBike { get => _isEnabledSelectSelectedAddBike; set { _isEnabledSelectSelectedAddBike = value; Notify("IsEnabledSelectSelectedAddBike"); } }
        public Boolean IsEnabledConnectBike { get => _isEnabledConnectBike; set { _isEnabledConnectBike = value; Notify("IsEnabledConnectBike"); } }
        //getter and setter Buttons to control the bikes in the stores
        public int SelectSelectedAddBike
        {
            get { return _selectSelectedAddBike; }
            set
            {
                _selectSelectedAddBike = value;
                _selectSelectedRemoveBike = -1;
                IsEnabledConnectBike = true;
            }
        }
        public int SelectSelectedRemoveBike
        {
            get { return _selectSelectedRemoveBike; }
            set
            {
                _selectSelectedRemoveBike = value;
                _selectSelectedAddBike = -1;
                IsEnabledConnectBike = false;
            }
        }
        public Retailer SelectedRetailer { get => _selectedRetailer; set { 
                _selectedRetailer = value; 
                Notify("SelectedRetailer");
                FilterOut filterOut = new FilterOut();
                ListOfBikesToInsert = filterOut.FilterOutBikes(Bikes, Retailers);
            } 
        }
        //relay functions
        public RelayCommand AddRetailerClick { get; set; }
        public RelayCommand ClearFormClick { get; set; }
        public RelayCommand DeleteRetailerClick { get; set; }
        public RelayCommand AddBikeToStoreClick { get; set; }
        public RelayCommand RemoveBikeToStoreClick { get; set; }
        public RetailerEditViewModel(ObservableCollection<Bike> bikeList, ObservableCollection<Retailer> retailerList)
        {
            AddRetailerClick = new RelayCommand(AddRetailer);
            ClearFormClick = new RelayCommand(ClearForm);
            DeleteRetailerClick = new RelayCommand(DeleteRetailer);
            AddBikeToStoreClick = new RelayCommand(AddBikeToStore);
            RemoveBikeToStoreClick = new RelayCommand(RemoveBikeToStore);
            Bikes = bikeList;
            Retailers = retailerList;
            SelectedRetailer = new Retailer(true);
            FilterOut filterOut = new FilterOut();
            ListOfBikesToInsert = filterOut.FilterOutBikes(Bikes, Retailers);
        }
        public void AddRetailer(object a)
        {
            SelectedRetailer.RetailerId = Search.SearchForEmptyIdByRetailer(Retailers);
            SelectedRetailer.IsEnabled = false;
            Retailers.Add(SelectedRetailer);
            SelectedRetailer = new Retailer(true);
        }
        public void ClearForm(object a)
        {
            SelectedRetailer = new Retailer(true);
        }
        public void DeleteRetailer(object a)
        {
            Retailers.Remove(SelectedRetailer);
            SelectedRetailer = new Retailer(true);
        }
        public void AddBikeToStore(object a)
        {
            Validator valid = new Validator();
            Boolean allowed = valid.ControllListBoxItemsOfBikes(SelectedBike, SelectedRetailer);
            if(allowed == false)
            {
                if(SelectedRetailer.Bikes == null)
                {
                    SelectedRetailer.Bikes = new ObservableCollection<Bike> { SelectedBike };
                }
                else
                {
                    SelectedRetailer.Bikes.Add(SelectedBike);
                }
                ListOfBikesToInsert.Remove(SelectedBike);
            }
        }
        public void RemoveBikeToStore(object a)
        {
            Validator valid = new Validator();
            Boolean allowed = valid.ControllListBoxItemsOfBikes(SelectedBike, SelectedRetailer);
            if (allowed == false)
            {
                ListOfBikesToInsert.Add(SelectedBike);
                SelectedRetailer.Bikes.Remove(SelectedBike);
            }
        }
        private void Notify(string v)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(v));
        }
    }
}
