﻿using BikeRental.Class;
using BikeRental.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BikeRental.ViewModel
{
    class CustomerViewModel : INotifyPropertyChanged
    {
        private Customer _selectedCustomer;
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Customer> Customers { get; set; }
        public ObservableCollection<Bike> Bikes { get; set; }
        public Customer SelectedCustomer { get => _selectedCustomer; set { _selectedCustomer = value; Notify("SelectedCustomer"); } }
        public RelayCommand AddCustomerClick { get; set; }
        public RelayCommand ClearFormClick { get; set; }
        public RelayCommand DeleteCustomerClick { get; set; }
        public CustomerViewModel(ObservableCollection<Customer> customersList, ObservableCollection<Bike> bikesList)
        {
            AddCustomerClick = new RelayCommand(AddCustomer);
            ClearFormClick = new RelayCommand(ClearForm);
            DeleteCustomerClick = new RelayCommand(DeleteCustomer);
            SelectedCustomer = new Customer(true);
            Customers = customersList;
            Bikes = bikesList;
        }

        private void DeleteCustomer(object b)
        {
            Customers.Remove(SelectedCustomer);
            SelectedCustomer = new Customer(true);
        }

        private void ClearForm(object b)
        {
            SelectedCustomer = new Customer(true);
        }

        private void AddCustomer(object b)
        {
            SelectedCustomer.CustomerId = Search.SearchForEmptyIdByCustomer(Customers);
            SelectedCustomer.IsEnabled = false;
            Customers.Add(SelectedCustomer);
            SelectedCustomer = new Customer(true);
        }
        private void Notify(string v)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(v));
        }
        public IList<SexGender> SexList
        {
            get
            {
                return Enum.GetValues(typeof(SexGender)).Cast<SexGender>().ToList<SexGender>();
            }
        }
    }
}

