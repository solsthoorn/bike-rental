﻿using BikeRental.Model;
using BikeRental.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BikeRental.View
{
    /// <summary>
    /// Interaction logic for ReservationView.xaml
    /// </summary>
    public partial class ReservationView : Window
    {
        public ReservationView(ObservableCollection<Customer> customers, ObservableCollection<Bike> bikes, ObservableCollection<Retailer> retailers, ObservableCollection<Reservation> reservations)
        {
            InitializeComponent();
            DataContext = new ReservationsViewModel(customers, bikes, retailers, reservations);
        }
    }
}
