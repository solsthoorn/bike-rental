﻿using BikeRental.Model;
using BikeRental.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BikeRental.View
{
    /// <summary>
    /// Interaction logic for RetailersView.xaml
    /// </summary>
    public partial class RetailersView : Window
    {
        public RetailersView(ObservableCollection<Bike> bikes, ObservableCollection<Retailer> retailers)
        {
            InitializeComponent();
            DataContext = new RetailerEditViewModel(bikes, retailers);
        }
    }
}
